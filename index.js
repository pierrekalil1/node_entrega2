import jwt, { decode } from "jsonwebtoken";
import express from "express";
import { v4 as uuidv4 } from "uuid";
import * as yup from "yup";
import * as bcrypt from "bcryptjs";

const app = express();
const PORT = 3000;
const config = {
  secret: "my_random_secret_key",
  expiresIn: "1h",
};
const schema = yup.object().shape({
  email: yup.string().email().required(),
  username: yup.string().required().min(2),
  age: yup.number().required().positive().integer(),
  password: yup.string().required(),
  id: yup
    .string()
    .default(() => {
      return uuidv4();
    })
    .transform(() => {
      return uuidv4();
    }),
  createdOn: yup
    .date()
    .default(() => {
      return new Date();
    })
    .transform(() => {
      return new Date();
    }),
});

const validate = (schema) => async (req, res, next) => {
  const resource = req.body;
  try {
    const validatedData = await schema.validate(resource, {
      abortEarly: false,
      stripUnknown: true,
    });
    req.validatedData = validatedData;
    next();
  } catch (e) {
    return res.status(422).json({ message: e.errors[0] });
  }
};

const authenticateUser = (req, res, next) => {
  let token = req.headers.authorization.split(" ")[1];
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Invalid Token." });
    }
    req.user = decoded;
  });
  return next();
};

const authorize = async (req, res, next) => {
  const { id } = req.params;
  const currentUser = users.find((item) => item.name === req.user.name);
  try {
    if (currentUser.id === id) {
      let user = users.find((user) => user.id === id);
      const hashedPassword = await bcrypt.hash(req.body.password, 10);
      user["password"] = hashedPassword;
      res.status(204).json({ password: req.body });
    } else {
      res.status(403).json({ message: "Is not permission" });
    }
  } catch (e) {
    console.log(e);
  }
  next();
};

const users = [];
app.use(express.json());

app.post("/signup", validate(schema), async (req, res) => {
  try {
    const data = req.validatedData;

    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    data.password = hashedPassword;

    const { password: data_password, ...dataWithoutPassword } = data;
    users.push(data);
    res.status(201).json(dataWithoutPassword);
  } catch (e) {
    res.status(422).json({ message: "Error while creating an user" });
  }
});

app.post("/login", async (req, res) => {
  let { username, password } = req.body;
  let user = users.find((user) => user.username === username);

  try {
    const match = await bcrypt.compare(password, user.password);
    let token = jwt.sign({ username: username, uuid: user.id }, config.secret, {
      expiresIn: config.expiresIn,
    });
    if (match) {
      res.status(200).json({ accessToken: token });
    } else {
      res.status(401).json({ message: "Invalid Credentials" });
    }
  } catch (e) {
    console.log(e);
  }
});

app.put(
  "/users/:id/password",
  authenticateUser,
  authorize,
  async (req, res) => {
    // const lal = require("os").userInfo();
    // console.log(lal, "*************************************************");
    const { password } = req.body;
    res.status(204).json({ password: password });
  }
);

app.get("/users", authenticateUser, (req, res) => {
  res.json(users);
});

app.listen(3000, () => {
  console.log(`Running at http://localhost:3000`);
});
