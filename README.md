# Entrega 2

A [Entrega 2] sistema de cadastro de usuarios com validações de senha e formulário.

## Instalação/Utilização

Para ter acesso à estrutura da API, faça o fork e depois clone este projeto.

## Rotas

<h3 align='center'> Cadastro de usuário</h3>

`POST /signup - para cadastro de usuários FORMATO DA REQUISIÇÃO `

```json
{
  "username": "Pierre",
  "email": "Pierre@kenzie.com",
  "password": "123zbc",
  "age": 35
}
```

Caso dê tudo certo, a resposta será assim:

`POST /signup - FORMATO DA RESPOSTA - STATUS 201`

```json
{
  "createdOn": "2022-01-13T17:33:22.118Z",
  "id": "09356b9c-75cd-4c84-b644-aa9af6098c2a",
  "age": 35,
  "username": "Pierre",
  "email": "pierre@mail.com"
}
```

<h3 align='center'> Login de usuário</h3>
`POST /login - para login de usuários FORMATO DA REQUISIÇÃO `

```json
{
  "username": "Pierre",
  "password": "123abc"
}
```

Caso dê tudo certo, a resposta será assim:

`POST /login - FORMATO DA RESPOSTA - STATUS 200`

```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRnYWJyaWVsYSIsImlhdCI6MTY0MjAzMDE0NywiZXhwIjoxNjQyMDMzNzQ3fQ.gCL0E0tCmK-pDOYDDz5c6imItL3v9ndwJQfL9-yv12I"
}
```

<h3 align='center'> Buscar usuários</h3>
`GET /users -  FORMATO DA REQUISIÇÃO `

Caso dê tudo certo, a resposta será assim:

`GET /users - FORMATO DA RESPOSTA - STATUS 200`

```json
[
  {
    "username": "Pierre",
    "age": 35,
    "email": "Pierre@mail.com",
    "createdOn": "2022-01-12T23:50:07.614Z",
    "password": "$2a$10$WKAcFjd15Co20xoZEIKafOzVk2wLWQiOcXG0KIsXSlWkXS4A1qtga",
    "id": "f538262c-8c49-4c6c-a663-200c3c91ae10"
  }
]
```

<h3 align='center'> Editar usuário</h3>
`PUT /users/:id -  FORMATO DA REQUISIÇÃO `
Authorization: Bearer {token}

Caso dê tudo certo a resposta será assim:

`Put /users/:id - FORMATO DA RESPOSTA - 204`

```json
{
  "password": "321qwe"
}
```

`
